# Mettez ElasticSearch à votre service

Une conférence réalisée lors de [PyconFR 2019](https://pycon.fr) à Bordeaux.

Le support est disponible: [Mettez Elastic à votre service](Mettez Elastic à votre service.slides.html)

## Modifier

La présentation est sous forme d'un notebook.

Vous pouvez ensuite lancer le notebook en utilisant:

```
$ docker-compose build
$ docker-compose up
```

puis en ouvrant votre navigateur sur http://127.0.0.1:8888/
