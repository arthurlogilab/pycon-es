FROM jupyter/minimal-notebook:latest

USER root

# PYTHON
RUN set -x && \
    pip install -U pip && \
    pip install elasticsearch-dsl lxml luqum && \
    pip install  RISE

USER jovyan
