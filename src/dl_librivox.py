from urllib.error import HTTPError
import json
import urllib.request

from lxml import etree


def xml_books():
    BASE_URL = "https://librivox.org/api/feed/audiobooks?limit=%d&offset=%d"   
    LIMIT = 100
    offset = 0
    while True:
        try:
            with urllib.request.urlopen(BASE_URL % (LIMIT, offset)) as conn:
                doc = conn.read()
        except HTTPError:
            break
        if doc.startswith(b"<xml><error>"):
            break
        xml = etree.fromstring(doc)
        for books in xml:
            for book in books:
                yield book
        offset += LIMIT


def parse_authors(authors):
    for author in authors:
        data = {elt.tag: elt.text for elt in author}
        for k in ["id"]:
            data[k] = int(data[k])
        yield data


def parse_book(book):
    data = {}
    for elt in book:
        if elt.tag == "authors":
            data["authors"] = list(parse_authors(elt))
        else:
            data[elt.tag] = elt.text
    for k in ["id", "num_sections"]:
        data[k] = int(data[k])
    if data.get("totaltime"):
        named_totaltime = zip(["hours", "minutes", "seconds"], data["totaltime"].split(":"))
        data["totaltime"] = {k: int(v) for k, v in named_totaltime}
    return data


if __name__ == "__main__":
    for b in xml_books():
        print(json.dumps(parse_book(b)))
